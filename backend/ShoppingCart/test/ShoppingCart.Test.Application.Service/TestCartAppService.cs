using Xunit;
using Moq;
using ShoppingCart.Application.Service;
using System.Collections.Generic;
using ShoppingCart.Domain.Domain;
using System.Linq;
using ShoppingCart.Domain.Domain.Enum;

namespace ShoppingCart.Test.Application.Service
{
    public class TestCartAppService
    {
        public CartAppService _appService { get; set; }
        public TestCartAppService() {
            this._appService = new CartAppService();
        }

        [Theory]
        [InlineData(3, 4, 1600)]
        [InlineData(4, 2, 2000)]
        [InlineData(1, 5, 500)]
        public void CheckTotalValues(int articleId, int quantity, int total) {
            var articles = this.GetArticles();

            var carts = new List<CartDomain> {
                new CartDomain() { 
                    Id = 1, 
                    Items = new List<ItemDomain> { 
                        new ItemDomain {
                            ArticleId = articleId,
                            Quantity = quantity
                        } 
                    } 
                }
            };

            var respnse = this._appService.CalculateTotalByCart(articles, carts);

            Assert.Equal(respnse.Single().Total, total);
        }

        [Theory]
        [InlineData(3000, 3000)]
        [InlineData(1500, 1900)]
        [InlineData(50, 850)]
        public void CheckTotalValuesWithDeliveryFeels(int total, int totalWithDeliveryFeel) {
            
            var carts = new List<CartDomain> {
                new CartDomain() { 
                    Id = 1,
                    Total = total
                }
            };

            var deliveryFeels = new List<DeliveryFeeDomain> {
                new DeliveryFeeDomain {
                    EligibleTransactionVolume = new EligibleTransactionVolumeDomain { MinPrice = 0, MaxPrice = 1000 },
                    Price = 800
                },
                new DeliveryFeeDomain {
                    EligibleTransactionVolume = new EligibleTransactionVolumeDomain { MinPrice = 1000, MaxPrice = 2000 },
                    Price = 400
                },
                new DeliveryFeeDomain {
                    EligibleTransactionVolume = new EligibleTransactionVolumeDomain { MinPrice = 2000, MaxPrice = null },
                    Price = 0
                }
            };

            var respnse = this._appService.CalculateDeliveryFees(carts, deliveryFeels);
            
            Assert.Equal(respnse.Single().Total, totalWithDeliveryFeel);
        }

        [Theory]
        [InlineData(DiscountType.AMOUNT, 25, 75)]
        [InlineData(DiscountType.PERCENTAGE, 50, 50)]
        public void CheckDiscountValue(DiscountType type, int value, int total) {
            var articles = this.GetArticles();
            var discounts = new List<DiscountDomain> {
                new DiscountDomain() { ArticleId = 1, Type = type, Value = value }
            };

            var result = this._appService.ApplyDiscount(discounts, articles).FirstOrDefault();
            Assert.Equal(result.Price, total);
        }

        private IEnumerable<ArticleDomain> GetArticles() {
            return new List<ArticleDomain> {
                new ArticleDomain() { Id = 1, Name = "water", Price = 100 },
                new ArticleDomain() { Id = 2, Name = "honey", Price = 200 },
                new ArticleDomain() { Id = 3, Name = "mango", Price = 400 },
                new ArticleDomain() { Id = 4, Name = "tea", Price = 1000 },
            };
        }
    }
}