using Xunit;
using Moq;
using AutoMapper;
using ShoppingCart.Api.Controllers.v1;
using ShoppingCart.Domain.AppService;
using ShoppingCart.Domain.Domain;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ShoppingCart.Api.ViewModel;
using System.Linq;

namespace ShoppingCart.Test.Api.Controller
{
    public class TestCartController
    {
        private CartController _controller { get; set; }
        private Mock<ICartAppService> _cartAppServiceMock { get; set; }
        private IMapper _mapper { get; set; }

        public TestCartController() {
            var mapperConfig = new MapperConfiguration(c => {
                c.AddProfile(new ShoppingCart.Api.MapperConfig.MappingProfile());
            });
            this._mapper = mapperConfig.CreateMapper();
            this._cartAppServiceMock = new Mock<ICartAppService>();

            this._controller = new CartController(this._mapper, this._cartAppServiceMock.Object);
        }

        [Fact]
        public void MappingTest() {
            this._cartAppServiceMock.Setup(x => 
                x.CalculateTotalByCart(It.IsAny<IEnumerable<ArticleDomain>>(), It.IsAny<IEnumerable<CartDomain>>())
            ).Returns(new List<CartDomain>() {
                new CartDomain(),
                new CartDomain(),
                new CartDomain()
            });

            var data = new InputDataViewModel() {
                Articles = new List<ArticleViewModel>() {
                    new ArticleViewModel() { Id = 1, Name = "water", Price = 100 },
                    new ArticleViewModel() { Id = 2, Name = "honey", Price = 200 },
                    new ArticleViewModel() { Id = 3, Name = "mango", Price = 400 },
                    new ArticleViewModel() { Id = 4, Name = "tea", Price = 1000 },
                },
                Carts = new List<CartViewModel>() {
                    new CartViewModel(),
                    new CartViewModel(),
                    new CartViewModel()
                }
            };

            var result = this._controller.CalculateTotal(data);
            var okResult = result as OkObjectResult;
            var responseBody = okResult.Value as OutputDataViewModel;
            
            Assert.Equal(data.Carts.Count(), responseBody.Carts.Count());
        }
    }
}