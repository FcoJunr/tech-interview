using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ShoppingCart.Domain.AppService;
using ShoppingCart.Application.Service;

namespace ShoppingCart.Infra.CrossCutting.Ioc
{
    public static class BootStrapper
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ICartAppService, CartAppService>();
        }
    }
}