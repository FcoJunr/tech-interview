using System.Collections.Generic;
using System.Linq;
using ShoppingCart.Domain.AppService;
using ShoppingCart.Domain.Domain;

namespace ShoppingCart.Application.Service
{
  public class CartAppService : ICartAppService
  {
    public IEnumerable<CartDomain> CalculateTotalByCart(IEnumerable<ArticleDomain> articles, IEnumerable<CartDomain> carts)
    {
        foreach(var cart in carts) {
            cart.Total = 0;
            foreach(var item in cart.Items) {
                cart.Total += articles
                    .Where(x => x.Id == item.ArticleId)
                    .SingleOrDefault().Price * item.Quantity;
            }
        }

        return carts;
    }

    public IEnumerable<CartDomain> CalculateDeliveryFees(IEnumerable<CartDomain> carts, IEnumerable<DeliveryFeeDomain> deliveryFeels)
    {
        foreach (var cart in carts) {
            cart.Total += deliveryFeels
                .Where(x => x.EligibleTransactionVolume.IsWithinTransactionRange(cart.Total))
                .FirstOrDefault()
                .Price;
        }

        return carts;
    }

    public IEnumerable<ArticleDomain> ApplyDiscount(IEnumerable<DiscountDomain> discounts, IEnumerable<ArticleDomain> articles)
    {
        foreach(var article in articles) {
            var discount = discounts.Where(x => x.ArticleId == article.Id).SingleOrDefault();
            if (discount != null) {
                article.Price = article.GetPriceWithDiscount(discount.Type, discount.Value);
            }
        }
        return articles;
    }
  }
}