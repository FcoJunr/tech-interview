using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShoppingCart.Api.ViewModel;
using ShoppingCart.Domain.AppService;
using AutoMapper;
using ShoppingCart.Domain.Domain;
using System;

namespace ShoppingCart.Api.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class CartController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ICartAppService _cartAppService;

        public CartController(IMapper mapper, ICartAppService cartAppService) {
            _mapper = mapper;
            _cartAppService = cartAppService;
        }

        [HttpPost]
        public IActionResult CalculateTotal([FromBody]InputDataViewModel data) {
            try {
                if (!ModelState.IsValid) {
                    return BadRequest(new { Error = ModelState });
                }

                var articles = this._mapper.Map<IEnumerable<ArticleDomain>>(data.Articles);

                if (data.Discounts != null) {
                    var discounts = this._mapper.Map<IEnumerable<DiscountDomain>>(data.Discounts);
                    articles = this._cartAppService.ApplyDiscount(discounts, articles);
                }

                var carts = this._mapper.Map<IEnumerable<CartDomain>>(data.Carts);
                carts = this._cartAppService.CalculateTotalByCart(articles, carts);

                if(data.DeliveryFees != null) {
                    var deliveryFeels = this._mapper.Map<IEnumerable<DeliveryFeeDomain>>(data.DeliveryFees);
                    carts = this._cartAppService.CalculateDeliveryFees(carts, deliveryFeels);
                }

                return Ok(new OutputDataViewModel() { Carts = this._mapper.Map<IEnumerable<CartTotalViewModel>>(carts) });
            } catch(Exception ex) {
                return StatusCode(500, new { Error = ex.Message });
            }
        }
    }
}