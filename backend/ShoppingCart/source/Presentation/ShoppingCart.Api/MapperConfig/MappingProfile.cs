using AutoMapper;
using ShoppingCart.Domain.Domain;
using ShoppingCart.Api.ViewModel;

namespace ShoppingCart.Api.MapperConfig
{
    public class MappingProfile : Profile {
        public MappingProfile() {
            CreateMap<ArticleDomain, ArticleViewModel>().ReverseMap();
            CreateMap<CartDomain, CartViewModel>().ReverseMap();
            CreateMap<CartDomain, CartTotalViewModel>().ReverseMap();
            CreateMap<ItemDomain, ItemViewModel>().ReverseMap();
            CreateMap<DeliveryFeeDomain, DeliveryFeeViewModel>().ReverseMap();
            CreateMap<EligibleTransactionVolumeDomain, EligibleTransactionVolumeViewModel>().ReverseMap();
            CreateMap<DiscountDomain, DiscountViewModel>().ReverseMap();
        }
    }
}