using System.Collections.Generic;

namespace ShoppingCart.Api.ViewModel
{
    public class OutputDataViewModel
    {
        public IEnumerable<CartTotalViewModel> Carts { get; set; }
    }
}