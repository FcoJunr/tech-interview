using System.ComponentModel.DataAnnotations;

namespace ShoppingCart.Api.ViewModel
{
    public class ItemViewModel
    {
        [Required]
        public int ArticleId { get; set; }

        public int Quantity { get; set; }
    }
}