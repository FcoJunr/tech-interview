namespace ShoppingCart.Api.ViewModel
{
    public class EligibleTransactionVolumeViewModel
    {
        public int MinPrice { get; set; }

        public int? MaxPrice { get; set; }
    }
}