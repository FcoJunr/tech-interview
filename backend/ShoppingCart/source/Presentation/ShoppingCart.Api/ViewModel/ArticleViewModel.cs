namespace ShoppingCart.Api.ViewModel
{
    public class ArticleViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public int Price { get; set; }
    }
}