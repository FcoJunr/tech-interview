using System.Collections.Generic;

namespace ShoppingCart.Api.ViewModel
{
    public class CartViewModel
    {
        public CartViewModel() {
            this.Items = new HashSet<ItemViewModel>();
        }

        public int Id { get; set; }
        
        public IEnumerable<ItemViewModel> Items { get; set; }
    }
}