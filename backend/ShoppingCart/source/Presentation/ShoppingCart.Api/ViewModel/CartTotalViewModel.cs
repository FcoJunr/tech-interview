namespace ShoppingCart.Api.ViewModel
{
    public class CartTotalViewModel
    {
        public int Id { get; set; }
        public int Total { get; set; }
    }
}