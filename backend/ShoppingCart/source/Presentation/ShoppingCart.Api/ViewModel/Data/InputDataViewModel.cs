using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShoppingCart.Api.ViewModel
{
    public class InputDataViewModel
    {
        [Required]
        public IEnumerable<ArticleViewModel> Articles { get; set; }

        [Required]
        public IEnumerable<CartViewModel> Carts { get; set; }

        public IEnumerable<DeliveryFeeViewModel> DeliveryFees { get; set; }

        public IEnumerable<DiscountViewModel> Discounts { get; set; }
    }
}