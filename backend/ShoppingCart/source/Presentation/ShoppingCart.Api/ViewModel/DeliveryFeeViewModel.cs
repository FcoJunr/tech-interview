namespace ShoppingCart.Api.ViewModel
{
    public class DeliveryFeeViewModel
    {
        public EligibleTransactionVolumeViewModel EligibleTransactionVolume { get; set; }
        
        public int Price { get; set; }
    }
}