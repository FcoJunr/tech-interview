using System.ComponentModel.DataAnnotations;
using ShoppingCart.Domain.Domain.Enum;

namespace ShoppingCart.Api.ViewModel
{
    public class DiscountViewModel
    {
        [Key]
        public int ArticleId { get; set; }
        public int Value { get; set; }
        public DiscountType Type { get; set; }
    }
}