using ShoppingCart.Domain.Domain.Enum;

namespace ShoppingCart.Domain.Domain
{
    public class ArticleDomain : DomainBase
    {
        public string Name { get; set; }
        public double Price { get; set; }

        public double GetPriceWithDiscount(DiscountType type, double value) {
            switch (type) {
                case DiscountType.AMOUNT:
                    return this.Price - value;
                case DiscountType.PERCENTAGE:
                    return this.Price - ((this.Price * value) / 100);
                default:
                    return this.Price;
            }
        }
    }
}