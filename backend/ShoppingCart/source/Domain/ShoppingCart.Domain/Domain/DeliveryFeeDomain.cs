namespace ShoppingCart.Domain.Domain
{
    public class DeliveryFeeDomain
    {
        public EligibleTransactionVolumeDomain EligibleTransactionVolume { get; set; }
        
        public int Price { get; set; }
    }
}