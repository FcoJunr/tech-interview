namespace ShoppingCart.Domain.Domain
{
    public class ItemDomain : DomainBase
    {
        public int Quantity { get; set; }
        public int ArticleId { get; set; }
    }
}