namespace ShoppingCart.Domain.Domain.Enum
{
    public enum DiscountType
    {
        AMOUNT,
        PERCENTAGE
    }
}