using System;
using System.Collections.Generic;

namespace ShoppingCart.Domain.Domain
{
    public class CartDomain : DomainBase
    {
        private double _Total { get; set; }
        public ICollection<ItemDomain> Items { get; set; }
        public double Total { 
            set {
                this._Total = value;
            }
            get {
                return Math.Floor(this._Total);
            }
        }
    }
}