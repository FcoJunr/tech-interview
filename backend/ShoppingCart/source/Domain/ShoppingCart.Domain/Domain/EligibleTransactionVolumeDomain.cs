namespace ShoppingCart.Domain.Domain
{
    public class EligibleTransactionVolumeDomain
    {
        public int MinPrice { get; set; }

        public int? MaxPrice { get; set; }

        public bool IsWithinTransactionRange(double value) {
            if (this.MaxPrice != null) {
                return value >= this.MinPrice && value < this.MaxPrice;
            } else {
                return value >= this.MinPrice;
            }
        }
    }
}