using ShoppingCart.Domain.Domain.Enum;

namespace ShoppingCart.Domain.Domain
{
    public class DiscountDomain
    {
        public int ArticleId { get; set; }
        public DiscountType Type { get; set; }
        public int Value { get; set; }
    }
}