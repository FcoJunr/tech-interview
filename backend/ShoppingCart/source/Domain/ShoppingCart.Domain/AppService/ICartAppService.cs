using System.Collections.Generic;
using ShoppingCart.Domain.Domain;

namespace ShoppingCart.Domain.AppService
{
    public interface ICartAppService
    {
        IEnumerable<CartDomain> CalculateTotalByCart(IEnumerable<ArticleDomain> articles, IEnumerable<CartDomain> carts);
        IEnumerable<CartDomain> CalculateDeliveryFees(IEnumerable<CartDomain> carts, IEnumerable<DeliveryFeeDomain> deliveryFeels);
        IEnumerable<ArticleDomain> ApplyDiscount(IEnumerable<DiscountDomain> discounts, IEnumerable<ArticleDomain> articles);
    }
}