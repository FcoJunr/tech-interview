# ShoppingCart
Application developed in Linux environment with .Net Core 2.1, Visual Code and has its architecture based on Domain Drive Design.

To download the Postman collection click [here](https://bitbucket.org/FcoJunr/tech-interview/downloads/Tech%20Interview.postman_collection.json).

The following instructions follow for environments where Visual Code is used for the development of applications with .Net Core.

## Run
To run the application in your root folder run the command
```
dotnet run -p source/Presentation/ShoppingCart.Api/ShoppingCart.Api.csproj
```
or run `dotnet run` inside the` ShoppingCart.Api` directory.

## Tests
The unit tests are in the `test` folder, to start them execute ` dotnet test` in the terminal inside their respective directory.